<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Admin</title>

    <!-- Custom fonts for this template -->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-table fa-fw"></i>  Master Data Pixel</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <a href="index.php?p=addmasterpixel" class="btn btn-primary"><i class="fa fa-plus fa-fw"></i> Add</a>
                                <br><br>
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Chars</th>
                                            <th>Value</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php include ('koneksi.php') ?>
                                    <?php $ambil=$conn->query("SELECT * FROM tb_masterpixel"); ?>
                                    <?php while ($pecah=$ambil->fetch_assoc()){ ?>
                                        <tr>
                                            <td><?php echo $pecah['id_pixel']?></td>
                                            <td><?php echo $pecah['name_char']?></td>
                                            <td><?php echo $pecah['value_char']?></td>
                                            <td>
                                                 <a href="index.php?p=deletemasterpixel&id=<?php echo $pecah['id_pixel']; ?>" class="btn btn-danger"><i class="fa fa-trash fa-fw"></i>Delete</a>
                                                <a href="index.php?p=editmasterpixel&id=<?php echo $pecah['id_pixel']; ?>" class="btn btn-warning"><i class="fa fa-file fa-fw"></i> Edit</a>
                                            </td>
                                        </tr>
                                        <?php ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin-2.min.js"></script>

</body>

</html>