<div class="container-fluid">
    <!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 mb-4">
        <!-- Approach -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-home fa-fw"></i>  Dashboard</h6>
            </div>
            <div class="card-body">
                 <p class="text-justify">Hi all, Welcome to the admin page. This page is a page for admin to do data management for some needs. There are several menus available in the menu list to add or edit data requirements. You can add or edit required data and delete unnecessary data. If you want to be a part of our team, please do not violate and still comply with the existing provisions by maintaining our privacy.</p>
                 <p class="mb-0">We will be happy to welcome you to be a part of our team and ready to help and work well together. Thank you for wanting to be a part of us. Hope you enjoy and can do a good job.</p>
                <p class="mb-0"></p>
            </div>
        </div>
    </div>
</div>
</div>

    <!-- Page Wrapper -->
    <div id="wrapper">

            <!-- Main Content -->
            <div id="content">

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary"><i class="fa fa-book fa-fw"></i>  Logs History</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Email</th>
                                            <th>URL</th>
                                            <th>Title</th>
                                            <th>Description</th>
                                            <th>Keyword</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php include ('koneksi.php') ?>
                                    <?php $ambil=$conn->query("SELECT * FROM tb_log"); ?>
                                    <?php while ($pecah=$ambil->fetch_assoc()){ ?>
                                        <tr>
                                            <td><?php echo $pecah['id_log']?></td>
                                            <td><?php echo $pecah['email']?></td>
                                            <td><?php echo $pecah['url_website']?></td>
                                            <td><?php echo $pecah['title']?></td>
                                            <td><?php echo $pecah['description']?></td>
                                            <td><?php echo $pecah['keyword']?></td>
                                            <td><?php echo $pecah['heading_tags']?></td>
                                            <td><?php echo $pecah['google_analytics']?></td>
                                            <td><?php echo $pecah['favicon']?></td>
                                            <td><?php echo $pecah['canonicalization']?></td>
                                            <td>
                                                <a href="index.php?p=detailloghistory&id=<?php echo $pecah['id_log']; ?>" class="btn btn-primary"><i class="fa fa-eye fa-fw"></i></a>
                                            </td>
                                        </tr>
                                        <?php ?>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

    </div>