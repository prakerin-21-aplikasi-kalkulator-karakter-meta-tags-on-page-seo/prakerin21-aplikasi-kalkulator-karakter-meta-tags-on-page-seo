<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="m-0 font-weight-bold text-primary"><i class="fa fa-table fa-fw"></i>  Master Data Pixel</h4>
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Chars</label>
                        <input type="name" class="form-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Value</label>
                        <input type="value" class="form-control" name="value" required>
                    </div>
                    
                    <a href="index.php?p=masterpixel" class="btn btn-primary"> Back</a> <input class="btn btn-primary" name="save" value="Save" type="submit">
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    if(isset($_POST['save'])){
        $id = $_SESSION['id'];
        $name_char = $_POST['name'];
        $value_char = $_POST['value'];
        $create_date = date("Y-m-d");
        $data = $conn->query("INSERT INTO tb_masterpixel
        (name_char,value_char,create_date,update_date,id_user) VALUES('$name_char','$value_char','$create_date','$id')");

    echo "<script>alert('Data has been saved');</script>";
    echo "<meta http-equiv='refresh' content='1;url=index.php?p=masterpixel'>";
}
?>