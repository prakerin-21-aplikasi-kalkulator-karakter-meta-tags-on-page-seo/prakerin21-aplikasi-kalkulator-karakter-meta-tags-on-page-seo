<?php
session_start();
    include 'koneksi.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <script
      src="https://kit.fontawesome.com/64d58efce2.js"
      crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="style.css" />
    <title>Login</title>
  </head>
  <body>
    <div class="container">
      <div class="forms-container">
        <div class="signin-signup">
          <form role="form" method="post" class="sign-in-form">
            <h2 class="title">Login</h2>
            <div class="input-field">
              <i class="fas fa-user"></i>
              <input type="text" name="user" placeholder="Username" />
            </div>
            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" name="pass" placeholder="Password" />
            </div>
             <button class="btn btn-primary" name="login">LOGIN</button>
          </form>
                  <?php
                    if(isset($_POST['login'])){
                    $ambil=$conn->query("SELECT * FROM tb_user WHERE username='$_POST[user]'
                    AND password='$_POST[pass]'");
                    $cocok=$ambil->num_rows;
                      if($cocok==1){
                        $a = $ambil->fetch_assoc();
                        $_SESSION['user']=$a['name'];
                        $_SESSION['id']=$a['id_user'];
                        echo "<div class='alert alert-info'>LOGIN SUKSES</div>";
                        echo "<meta http-equiv='refresh' content='1;index.php'>";
                        }else {
                        echo "<div class='alert alert-danger'>LOGIN GAGAL</div>";
                      echo "<meta http-equiv='refresh' content='1;login.php'>";
                     }
                   }
                ?>
        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          <div class="content">
            <h3>WELCOME</h3>
            <p>
              Please enter your username and password to enter the next page!
            </p>
          </div>
          <img src="img/login.svg" class="image" alt="" />
        </div>
      </div>
    </div>

    <script src="app.js"></script>
  </body>
</html>
