<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="m-0 font-weight-bold text-primary"><i class="fa fa-users fa-fw"></i>  User List</h4>
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Name</label>
                        <input type="name" class="form-control" name="name" required>
                    </div>
                    <div class="form-group">
                        <label>Username</label>
                        <input type="username" class="form-control" name="username" required>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>
                    <div class="form-group">
                        <label>Status</label>
                        <p><input type="radio" name="status" value="active" required>  active<p>
                        <p><input type="radio" name="status" value="inactive" required>  inactive<p>
                    </div>
                    
                    <a href="index.php?p=userlist" class="btn btn-primary"> Back</a> <input class="btn btn-primary" name="save" value="Save" type="submit">
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    if(isset($_POST['save'])){
        $name = $_POST['name'];
        $username = $_POST['username'];
        $password = $_POST['password'];
        $email = $_POST['email'];
        $status = $_POST['status'];
        $create_date = date("Y-m-d");
        $data = $conn->query("INSERT INTO tb_user
        (name,username,password,email,status,create_date,update_date) VALUES('$name','$username','$password','$email','$status','$create_date')");

    echo "<script>alert('Data has been saved');</script>";
    echo "<meta http-equiv='refresh' content='1;url=index.php?p=userlist'>";
}
?>