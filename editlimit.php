<?php
$q=$conn->query("SELECT * FROM tb_limitconfig
WHERE id_limit='$_GET[id]'");
$a=$q->fetch_assoc();
?>

<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="m-0 font-weight-bold text-primary"><i class="fa fa-calculator fa-fw"></i>  Limitation Data </h4>
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Limitation</label>
                        <input type="text" class="form-control" name="name" disabled value="<?php echo $a['name_limit']?>">
                    </div>
                    <div class="form-group">
                        <label>Value</label>
                        <input type="value" class="form-control" name="value" required value="<?php echo $a['value_limit']?>">
                    </div>
                    <a href="index.php?p=limit" class="btn btn-primary"> Back</a> <input class="btn btn-primary" name="save" value="Save" type="submit"> 
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    if(isset($_POST['save'])){
        $id = $_SESSION['id'];
        $update_date = date("Y-m-d");
        $conn->query("UPDATE tb_limitconfig SET value_limit='$_POST[value]',update_date='$update_date',id_user='$id' WHERE id_limit='$_GET[id]'");
    echo "<script>alert('Data has been changed');</script>";
    echo "<meta http-equiv='refresh' content='1;url=index.php?p=limit'>";
}
?>