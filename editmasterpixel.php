<?php
$q=$conn->query("SELECT * FROM tb_masterpixel
WHERE id_pixel='$_GET[id]'");
$a=$q->fetch_assoc();
?>

<div class="row">
    <div class="col-12 grid-margin stretch-card">
        <div class="card">
            <div class="card-body">
                <h4 class="m-0 font-weight-bold text-primary"><i class="fa fa-table fa-fw"></i>  Master Data Pixel</h4>
                <form method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label>Chars</label>
                        <input type="text" class="form-control" name="name" required value="<?php echo $a['name_char']?>">
                    </div>
                    <div class="form-group">
                        <label>Value</label>
                        <input type="value" class="form-control" name="value" required value="<?php echo $a['value_char']?>">
                    </div>
                    <a href="index.php?p=masterpixel" class="btn btn-primary"> Back</a> <input class="btn btn-primary" name="save" value="Save" type="submit">
                </form>
            </div>
        </div>
    </div>
</div>
<?php
    if(isset($_POST['save'])){
        $id = $_SESSION['id'];
        $update_date = date("Y-m-d");
        $conn->query("UPDATE tb_masterpixel SET name_char='$_POST[name]',value_char='$_POST[value]',update_date='$update_date',id_user='$id' WHERE id_pixel='$_GET[id]'");
    echo "<script>alert('Data has been changed');</script>";
    echo "<meta http-equiv='refresh' content='1;url=index.php?p=masterpixel'>";
}
?>